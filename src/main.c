//
// Created by Арслан Ефимов on 17.12.2023.
//

#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>
#define HEAP_SZ 4096
#define MALLOC_SZ 2048
#define SMALL_MALLOC_SZ 148

static void print_message(const char* s){
    printf("%s\n", s);
}
static struct block_header* get_header(void* contents){
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

//Обычное успешное выделение памяти.
void test_success_memory_allocate(){
    void* heap = heap_init(HEAP_SZ);
    assert(heap);
    print_message("---TEST1---");
    if(heap == NULL){
        print_message("Test is failed, failed to initialize heap");
        return;
    }

    debug_heap(stdout, heap);

    void* allocate = _malloc(SMALL_MALLOC_SZ);
    assert(allocate);
    if(allocate == NULL){
        print_message("Test is failed, failed to allocate memory");
        return;
    }

    debug_heap(stdout, heap);
    heap_term();
    munmap(heap, size_from_capacity((block_capacity) {.bytes = HEAP_SZ}).bytes);
    print_message("Test 1 is success!");
}

//Освобождение одного блока из нескольких выделенных.
void test_freeing_one_block_from_several_allocated(){
    print_message("---TEST2---");
    void* heap = heap_init(HEAP_SZ);
    if(heap == NULL){
        print_message("Test is failed, failed to initialize heap");
        return;
    }
    debug_heap(stdout, heap);
    void* block_1  = _malloc(MALLOC_SZ);
    void* block_2  = _malloc(MALLOC_SZ);
    void* block_3  = _malloc(MALLOC_SZ);

    if(!block_1 || !block_2 || !block_3){
        print_message("Test is failed, allocation error");
        return;
    }

    debug_heap(stdout, heap);
    _free(block_2);
    if(!block_1 || !block_3){
        print_message("Test is failed, one block interferes with another");
        return;
    }

    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = HEAP_SZ}).bytes);
    print_message("Test 2 is success!");

}

//Освобождение двух блоков из нескольких выделенных.
void test_freeing_double_block_from_several_allocated(){
    print_message("---TEST3---");
    void* heap = heap_init(HEAP_SZ);
    if(heap == NULL){
        print_message("Test is failed, failed to initialize heap");
        return;
    }
    debug_heap(stdout, heap);
    void* block_1  = _malloc(MALLOC_SZ);
    void* block_2  = _malloc(MALLOC_SZ);
    void* block_3  = _malloc(MALLOC_SZ);

    if(!block_1 || !block_2 || !block_3){
        print_message("Test is failed, allocation error");
        return;
    }

    debug_heap(stdout, heap);
    _free(block_1);
    _free(block_2);
    if(!block_3){
        print_message("Test is failed, some blocks interferes with another");
        return;
    }
    munmap(heap, size_from_capacity((block_capacity) {.bytes = HEAP_SZ}).bytes);
    print_message("Test 3 is success!");
}

//Память закончилась, новый регион памяти расширяет старый.
void test_growing_heap(){
    print_message("---TEST4---");
    void* heap = heap_init(HEAP_SZ);
    if(heap == NULL){
        print_message("Test is failed, failed to initialize heap");
        return;
    }
    debug_heap(stdout, heap);

    void* block_1 = _malloc(REGION_MIN_SIZE);
    void* block_2 = _malloc(MALLOC_SZ);

    if(!block_1 || !block_2){
        print_message("Test is failed, failed to allocation");
        return;
    }

    debug_heap(stdout, heap);

    struct block_header* header_1 = get_header(block_1);
    struct block_header* header_2 = get_header(block_2);

    if(!header_1 || !header_2){
        print_message("Test failed, header 1 or header 2 is null");
        return;
    }
    size_t capct = ((struct block_header*) heap)->capacity.bytes + header_1->capacity.bytes + header_2->capacity.bytes;
    munmap(heap, capct);
    print_message("Test 4 is success!");
}
//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
    void test_non_continuous_heap(){
    print_message("---TEST5---");
    void* allocator = mmap((void *) HEAP_START, 8, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    assert(allocator);
    void* filled_allocator = _malloc(SMALL_MALLOC_SZ);
    assert(filled_allocator);
    assert(allocator != filled_allocator);
    print_message("Test 5 is success!");
}

int main(){

    test_success_memory_allocate();
    print_message("---------------------------\n\n");
    test_freeing_one_block_from_several_allocated();
    print_message("---------------------------\n\n");
    test_freeing_double_block_from_several_allocated();
    print_message("---------------------------\n\n");
    test_growing_heap();
    print_message("---------------------------\n\n");
    test_non_continuous_heap();
    print_message("---------------------------\n\n");
    return 0;
}


